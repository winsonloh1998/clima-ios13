# Clima iOS13 (AppBrewery Course)
An iOS application that obtain weather condition and temperature by calling API.

## What I Have Learned
- Switch Image/Color in Light & Dark Mode
- Call API
- Access GPS Longitude and Latitude

## Screenshot - Light Mode
![Clima Light Mode Screenshot](https://bitbucket.org/winsonloh1998/clima-ios13/raw/71f05e3bbf389ac8cc3b88a00b4c11cffd065f8e/Screenshot/Clima-LightMode.png)

## Screenshot - Dark Mode
![Clima Dark Mode Screenshot](https://bitbucket.org/winsonloh1998/clima-ios13/raw/71f05e3bbf389ac8cc3b88a00b4c11cffd065f8e/Screenshot/Clima-DarkMode.png)